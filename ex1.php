<?php
  $entrada = readline('Introdueix des de l\'entrada standard: ');
  if ($entrada >= 1 && $entrada <= 10) {
	echo "Taules de multiplicar del " . $entrada . "\n=====================\n";
	for ($i = 0; $i <= 10; $i++) {
	  	echo "$entrada * $i = " . $i*$entrada . "\n";
	}
  }
?>
