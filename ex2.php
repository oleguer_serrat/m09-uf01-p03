<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
table, td, th {
  border: 2px solid red;
}

table {
  width: 20px;
  border-collapse: collapse;
}
</style>
<body>
    <h1>Tabulant informació</h1>
    <table>
        <?php
        $matriz[0] = "cougar";
        $matriz[1] = "ford";
        $matriz[2] = null;
        $matriz[3] = "2.500";
        $matriz[4] = "V6";
        $matriz[5] = 182;
        $i = 0;
        foreach ($matriz as $valor) {

            echo "<tr>";
            echo "<td>$i</td>";
            echo "<td>$valor</td>";
            echo "</tr>";
            $i++;
        }
        ?>
    </table>
    <br>
    <table>
        <?php
        $i = 0;
        while ($i <= 5) {
            echo "<tr>";
            echo "<td>$i</td>";
            echo "<td>$matriz[$i]</td>";
            echo "</tr>";
            $i++;
        }
        ?>

    </table>

</body>
</html>

