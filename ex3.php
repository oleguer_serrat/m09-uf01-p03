<?php
$num_usuari = '';
$num_random = rand(0,10);
$tirades = 4;
while ($num_usuari != $num_random && $tirades >= 0){
	$num_usuari = readline('Introdueix un numero del 0 al 10: ');
	if ($num_usuari >= 0 && $num_usuari <= 10) {
		if ($num_usuari < $num_random ) {
			echo "T'has quedat curt\n";
		} if ($num_usuari > $num_random ) {
			echo "T'has quedat llarg\n";
		}
		echo "Et queden $tirades tirades\n";
		$tirades--;
	} elseif ($num_usuari < 0 && $num_usuari > 10) {
		echo "El valor introduit esta fora de rang!!!!!\n";
	} else {
		echo "El valor introduit no es un numero\n";
	}
}
if ($tirades < 0) {
	echo "Has perdut el joc\n";
} else {
	echo "Has guanyat el joc!!!\n";
}
?>
