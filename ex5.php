<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="text-align: center;">
        <pre>
<?php
$altura = 10;
for ($i = 1; $i <= $altura; $i++) {

    for ($k = 1; $k <= 2 * $i - 1; $k++) {
        echo "*";
    }

    echo "<br>";
}
?>
        </pre>
    </div>
</body>

</html>