<?php
$dia_en = date('D');
$dia_cat = '';

if ($dia_en == 'Mon') {
    $dia_cat = 'Dilluns';
} elseif ($dia_en == 'Tue') {
    $dia_cat = 'Dimarts';
} elseif ($dia_en == 'Wed') {
    $dia_cat = 'Dimecres';
} elseif ($dia_en == 'Thu') {
    $dia_cat = 'Dijous';
} elseif ($dia_en == 'Fri') {
    $dia_cat = 'Divendres';
} elseif ($dia_en == 'Sat') {
    $dia_cat = 'Dissabte';
} elseif ($dia_en == 'Sun') {
    $dia_cat = 'Diumenge';
}

echo "El dia de la setmana és: $dia_cat";
?>
